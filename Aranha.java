
/**
 *
 * @author MAURO
 */

import java.io.*;
import java.net.*;
import java.util.*;

public class Aranha {

    private static String servidor = "localhost";
    private static int porta = 8857;
    ServerSocket conexaoServidor;

    static ArrayList<String> agentes = new ArrayList();

    public Aranha(int porta) {
        try {
            this.conexaoServidor = new ServerSocket(porta);
            System.out.println("Aguardando conexoes na porta " + porta);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) throws Exception {

        try {
            
            DatagramSocket clientSocket;
            
            while (true) {
                try {
                    
                    clientSocket = new DatagramSocket();
                    InetAddress IPAddress = InetAddress.getByName(servidor);

                    byte[] sendData = new byte[1024];
                    byte[] receiveData = new byte[1024];

                    String sentence = "ativo?";
                    sendData = sentence.getBytes();
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, porta);

                    System.out.println("Enviando pacote UDP para " + servidor + ":" + porta);
                    clientSocket.send(sendPacket);

                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    clientSocket.setSoTimeout(1000);
                    try {

                        clientSocket.receive(receivePacket);
                        String recebido = new String(receivePacket.getData());

                        String dados[] = recebido.split(Pattern.quote(";"));
                        String ip = dados[1];
                        int portaAgente = Integer.parseInt(dados[2]);
                        String tipo = dados[3];
                        String status = dados[4];
                        gravarEndereco(ip, portaAgente, tipo, status);

                    } catch (SocketTimeoutException e) {
                        continue;
                    }

                    clientSocket.close();

                } catch (IOException | NumberFormatException ex) {
                }
            }

        } catch (Exception e) {
        }

    }

    private void gravarEndereco(String ip, int porta, String tipo, String status) {
        Agente agente = new Agente(tipo, status, ip, porta);
        Dns.GravarAgente(agente);
    }
}

}
    
    
    
    
}
