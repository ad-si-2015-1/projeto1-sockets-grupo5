# Termo de Abertura do Projeto
# Projeto 1 - Agentes Inteligentes

### Versão
1.0

### Objetivos deste documento
Construir um software de agentes inteligentes, utilizando API de sockets.
    
### Justificativa do projeto
Requisito parcial para andamento da disciplina de Aplicações Distribuídas.

### Situação atual
O projeto encontra-se na fase de elaboração da documentação, e concomitantemente, a implementação do próprio software.

### Produtos e principais requisitos
- Rodar em ambientes Windows e Linux;
- Utilizar API de sockets;
- Utilizar para controle de versão o ambiente GitLab;
- Aranha (Crawler): Varre a subrede em busca de outros agentes, procurando identificar o IP e a classificação. Quando questionado, fornece somente o último agente localizado. É imune a zumbis. Somente a aranha pode localizar e classificar agentes, e não pode ser morta;
- Respondedor: Responde a desafios aritméticos fornecidos por um questionador;
- Questionador: Fornece desafios aritméticos solicitados e avalia se a resposta está correta;
- Zumbi: Envia um sinal que faz com que o agente que o contactou se torne um zumbi, ou seja, não responde e nem questiona desafios aritméticos;
- Caçador: Envia um sinal que faz com que o zumbi seja preso. É imune ao zumbi;
- Curador: Envia um sinal que faz com que o zumbi se cure. É imune a zumbis.

### Marcos
- Elaboração do Termo de Abertura do Projeto: 22/03/2015
- Elaboração de documentações auxiliares: 22/03/2015
- Encerramento do Projeto: 29/03/2015

### Partes interessadas do Projeto
#####- Equipe:
- Allan Braga (Documentador);
- Farid Chaud (Documentador);
- José Ricardo (Desenvolvedor);
- Mauro Henrique (Desenvolvedor);
- Miriã Laís (Líder de equipe e Documentadora).

#####- Auxílio e avaliação:
- Marcelo Akira.

### Restrições
A principal limitação do projeto refere-se ao tempo destinado à elaboração e implementação do mesmo.

### Premissas
O fator essencial para o sucesso do andamento do projeto, refere-se ao alinhamento e dedicação da equipe responsável pelo mesmo.

### Riscos
- Escassez do tempo;
- Disponibilidade da equipe
- Mudanças drásticas reconhecidas no decorrer do projeto;
- Priorizações incorretas;
- Fase má elaborada.

### Aprovações
Termo de Abertura aprovado em: 22/03/2015.