### Documento de Especificação de Requisitos
### Agentes Inteligentes utilizando Sockets


#### Universidade Federal de Goiás - UFG
#### Instituto de Informática - INF
#### Sistemas de Informação
#### Aplicações Distribuídas
#### 1º semestre/ 2015
-------------------------------------------------------------
> **Professor:**
- Marcelo Akira Inuzuka

> **Grupo:**
- Allan Braga
- Farid Chaud
- Mauro Henrique
- Miriã Laís


-------------------------------------------------------------

#### 1. Introdução

**1.1. Propósito do documento de requisitos**

Este documento tem como propósito especificar as funcionalidades, requisitos funcionais, não funcionais e seus respectivos  de uso, para fins de avaliação de viabilidade para futuro desenvolvimento. 


**1.2. Escopo do produto**

Na Inteligência Artificial, os agentes são definidos como programas de computadores que simulam o relacionamento humano, por fazerem 
exatamente o que uma outra pessoa faria por você. Os agentes constituem um sistema que pode servir como um mensageiro, pelo motivo de 
possuir algumas habilidades de especialistas, e ainda, são componentes de software que comunicam com seus pares por troca de mensagens 
em uma linguagem de comunicação de agentes.  

No contexto do projeto em execução, os agentes se comunicam no intuito de fornecer (questionador) e responder (respondedor) desafios 
aritméticos, sendo que, nesse ambiente também há outros agentes, que tem como missão impedir que objetivo seja alcançado (zumbis), 
há outros agentes que visam capturar (caçador) e curar (curador) os agentes infecciosos, além do agente responsável pela localização e 
identificação de cada agente (aranha).


**1.3. Definições, acrônimos e abreviações**
VERIFICAR SE ESTE ITEM PODE SER RETIRADO.

#### 2. Descrição Geral

**2.1. Perspectiva do Software**

O software deve funcionar tanto em ambiente Windows, quanto Linux. É contituído pelos relacionamentos de seis agentes, que se comunicam
por meio de sockets, utilizando TCP e UDP.

**2.2. Funções do Sofware**

- Verificar ambiente;
- Identificar IP;
- Classificar agentes;
- Lançar e resolver desafios aritméticos;
- Prender e curar agentes infecciosos.

**2.3. Características dos Agentes**

**2.3.1. Aranha:**

Varre a subrede em busca de outros agentes, procurando identificar o IP e a classificação. Quando questionado, fornece somente o último
agente localizado. É imune a zumbis, é o único que pode localizar e classificar agentes e não pode ser morto.

**2.3.2. Respondedor:**

Responde a desafios aritméticos fornecidos por um questionador.

**2.3..3. Questionador:**

Fornece desafios aritméticos solicitados e avalia se a resposta está correta.

**2.3.4. Zumbi:**

Envia um sinal que faz com que o agente que o contactou se torne um zumbi, ou seja, não responde e nem questiona desafios aritméticos.

**2.3.5. Caçador:**

Envia um sinal que faz com que o zumbi seja preso. É imune ao zumbi.

**2.3.6. Curador:**

Envia um sinal que faz com que o zumbi se cure. É imune a zumbis.  

**2.4. Restrições Gerais**

A interface gráfica não será priorizada na construção do software.


#### 3. Requisitos do Sistema

**3.1. Requisitos Funcionais**

Requisitos Funcionais descrevem explicitamente as funcionalidades e serviços do sistema. Nesta seção será documentado os principais requisitos funcionais da aplicação:

- A classe aranha deverá possuir uma lista;
- A lista contida na classe aranha irá guardar o endereço ip, porta e o tipo do agente encontrado por ela;
- O aranha não pode ser infectado pelo zumbi;
- O aranha não pode ser "morto";
- Apenas o aranha localiza e classifica agentes;
- Questionador envia desafio para respondedor;
- Desafio é composto de um cálculo matemático;
- Respondedor envia resposta para questionador;
- Questionador verifica se está correto e envia confirmação de "correto" ou "incorreto";
- Zumbi busca na lista da aranha um endereço para se conectar;
- Aranha fornece apenas o último registro da lista;
- Zumbi se conecta no agente o qual tem o endereço e o torna um questionador zumbi ou respondedor zumbi;
- Zumbi infecta através da inserção de um código que passa a identificar o agente como um agente infectado;
- Caçador é imune a infecção de zumbi;
- Caso o zumbi localize na lista da aranha um agente que não seja respondedor ou questionador, ele passa para o próximo registro mais recente;
- Ao ser infectado, o agente não questiona nem responde mais a um questionamento;
- Caçador também pesquisa a lista da aranha;
- Quando caçador identifica um agente tipo zumbi, ele fecha suas conexões o impedindo de se conectar a qualquer outro agente (mata o zumbi);
- Curador também pesquisa a lista da aranha;
- Ao identificar um agente infectado, curador realiza a desinfecção do agente e o mesmo passa a operar normalmente;
- Caçador não pode ser infectado pelo zumbi;
- Curador não pode ser infectado pelo zumbi.

**3.2. Requisitos não Funcionais**

Requisitos não funcionais diferentemente dos funcionais, não especifica o que o sistema fará, mas como ele fará. Abordam aspectos de qualidade importantes em um sistema de software. Nesta seção iremos identificar os principais requisitos não funcionais da aplicação:

**3.2.1. Requisitos Não Funcionais do Produto**

- Os agentes deverão se comunicar por meio de protocolos de rede (TCP e/ou UDP);
- O sistema não apresentará uma interface gráfica;
- O sistema deve informar ao usuário de forma visível as ações dos agentes(por meio de prints);

**3.2.2. Requisitos Não Funcionais Organizacionais**

- A aplicação será desenvolvida em linguagem java;
- O sistema será operado em máquina virtual linux;
- A documentação do projeto será realizada em paralelo ao desenvolvimento;

**3.2.3. Requisitos Não Funcionais Externos**

- Uso do GitLab como ferramenta colaborativa do projeto;


### 4. Casos de Uso

Arquivo anexado.

### 5. Diagramas

**5.1. Diagrama de Raias**

Arquivo anexado.

**5.2. Diagrama de Estados**

Arquivo anexado.



OBSERVAÇÃO: APÓS FINALIZADO, ESTE DOCUMENTO SERÁ ADICIONADO NA WIKI DO PROJETO.